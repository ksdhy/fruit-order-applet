// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()

  let {name,isEx,price,util,img,operator,id} = event

  let res;
  if(operator == 'del'){
    res = await cloud.database().collection('shop').doc(id).remove()
  }else if(operator == 'save'){
    res = await cloud.database().collection('shop').doc(id).update({
      data:{
        name,isEx,price,util,img
      }
    }) 
  }

 



  return {
    res
  }
}