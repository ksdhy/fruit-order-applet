const app =  getApp();
const db = wx.cloud.database()
Page({
  data:{
    list:[]
  },

  addCar(e){
    if(e.currentTarget.dataset.isex == 0){

      wx.showToast({title: '添加失败,商品缺货',icon:'none'});
    }else{
      wx.showToast({title: '添加成功,请在五分钟内确认下单',icon:'none'});
      let isExist = false  // 判断数据是否存在
      app.globalData.car.forEach(element => {
        if(element.name == e.currentTarget.dataset.name){
          element.price += e.currentTarget.dataset.price
          element.num++
          isExist = true  // 存在
          element.time = new Date().getTime()
        }
      });
      if(!isExist){ // 如果不存在 ， 就创建数据
        app.globalData.car.push({
          name:e.currentTarget.dataset.name,
          price:e.currentTarget.dataset.price,
          util:e.currentTarget.dataset.util,
          num:1,
          time:new Date().getTime()
        })
      }
    }
  }
  
  

  ,onLoad:function(options){
    let _this = this
    db.collection('shop').get({
      success: res => {
        _this.setData({
          list:res.data
        })
      }
    })
  }
})
