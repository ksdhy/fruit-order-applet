const app =  getApp();
const db = wx.cloud.database()
Page({
  data:{
    list:[]
  },

  edit:function(e){
    wx.navigateTo({url: '../edit/edit?id='+e.currentTarget.dataset.id});
  },

  onLoad:function(options){
    let _this = this
    db.collection('shop').get({
      success: res => {
        _this.setData({
          list:res.data
        })
      }
    })
  },
  onShow(){
    this.onLoad()
  }
})
