const db = wx.cloud.database()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    buttons: [{text: '取消'}, {text: '确定'}],
    dialogShow: false,
    imgChange:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let _this = this;
    db.collection('shop').where({
      '_id':options.id
    }).get({
      success: res => {
       let {_id,img,isEx,name,price,util} = res.data[0]
       _this.setData({
         _id,img,isEx,name,price,util
       })
      }
    })
  },

  del(){
    this.setData({
      dialogShow:true,
      title:'确定删除该商品吗？',
      operation:'del'
    })
  },

  async submit(e){
    let obj = {
      ...e.detail.value,
      img:this.data.img,
      id:this.data._id
    }
    this.setData({
      dialogShow:true,
      title:'确定保存修改？',
      operation:'save',
      editInfo:obj
    })
    
  },
// dialog框的确定按钮点击，执行相应操作
  async operator(e){
    let _this = this  // 保存this
    // 按了确认
    if(e.detail.index == 1){
      // 删除操作
      if(this.data.operation == 'del'){
        let res = await wx.cloud.callFunction({
          name:'shopEdit',
          data:{
            id:_this.data._id,
            operator:'del'
          }
        })
        if(res.result.res.stats.removed == 1){
          wx.showToast({title: '操作成功'});
          wx.cloud.deleteFile({
            fileList: [_this.data.img]
          }).then(res => {
            // handle success
            console.log(res.fileList)
          }).catch(error => {
            // handle error
          })
        }else{
          wx.showToast({title: '操作失败，请稍后再试',icon:'none'});
        }
        setTimeout(()=>{
          wx.navigateBack({
            delta: 1
          });
        },500)
        
      }
      // 保存修改
      if(this.data.operation == 'save'){
        let imgPath = _this.data.img;
        // 如果在修改信息的时候改变了图片，则进行图片上传
        if(this.data.imgChange){
          let uploadRes = await wx.cloud.uploadFile({
            cloudPath:new Date().getTime()+'.jpg',
            filePath: _this.data.img
          })
          imgPath = uploadRes.fileID
        }

        // 调用云函数进行修改
        let res = await wx.cloud.callFunction({
          name:'shopEdit',
          data:{
            ..._this.data.editInfo,
            img:imgPath,
            operator:'save'
          }
        })
        // 修改条数为1代表修改成功，提示
        if(res.result.res.stats.updated == 1){
          wx.showToast({title: '操作成功'});
        }else{ // 修改失败
          wx.showToast({title: '操作失败，请稍后再试',icon:'none'});
        }
        // 无论成功或失败都跳转上一个页面
        setTimeout(()=>{
          wx.navigateBack({
            delta: 1
          });
        },500)
      }
    }
    this.setData({dialogShow:false}) // 关闭弹窗
  },

  // 选择图片（还未上传）
  chooseImage:function(){
    let _this = this;
    const db = wx.cloud.database()
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success (res) {
        _this.setData({
          img:res.tempFilePaths[0],
          imgChange:true
        })
      }

    })
  },

// 上传图片
  upload(){ 
    let _this = this
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})