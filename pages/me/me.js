const app =  getApp();
const db = wx.cloud.database()
Page({

  /**
   * 页面的初始数据 
   */
  data: {
    buttons: [{text: '取消'}, {text: '确定'}],
    dialogShow: false,
    password:'',
    passwordsubmit:'',
    oneButton: [{text: '确定'}],
    showOneButtonDialog:false
  },

  openConfirm: function () {
    this.setData({
        dialogShow: true,
        password: '',
    })
},

getpassword(e){
  console.log(e);
  this.setData({
    passwordsubmit:e.detail.value
  })
},
tapDialogButton(e) {
    if(e.detail.item.text == '确定'){
      // 发送请求
      let _this = this
      db.collection('adminPass').get().then(res => {
       if(res.data[0].password != _this.data.passwordsubmit){
         wx.showToast({
           title: '密码错误',
           icon: 'none'
         });
       }
       else{
         wx.navigateTo({url: '../admin/admin'});
       }
      })
      // console.log(this.data.passwordsubmit);
    }
    this.setData({
        dialogShow: false,
    })
},




  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              this.setData({
                avatarUrl: res.userInfo.avatarUrl,
                userInfo: res.userInfo
              })
            }
          })
        }
      }
    })
  },
 // 判断条件进入购物车  时间和数量 （未完成）
  car(){
    if(app.globalData.car.length == 0){
      this.setData({
        showOneButtonDialog:true
      })
      return;
    }
 
    wx.navigateTo({
      url: '../car/car'
    });
  },

  carDialogClose(){
    this.setData({
      showOneButtonDialog:false
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})